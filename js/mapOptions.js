function initialize() {
  var mapOptions = {
	    center: new google.maps.LatLng(1.298824,103.853511),
	    zoom:16,
	    panControl:false,
	    zoomControl:false,
	    mapTypeControl:false,
	    scaleControl:false,
	    streetViewControl:false,
	    overviewMapControl:false,
	    rotateControl:false,
	    disableDoubleClickZoom: true,
	    scrollwheel:false,    
	    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("google-map"),mapOptions);

  var marker = new google.maps.Marker({
    	position: new google.maps.LatLng(1.298824,103.853511),
    	map: map,
    	title:"National Design Center"
  });
}
google.maps.event.addDomListener(window, 'load', initialize);